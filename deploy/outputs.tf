output "db_host" {
  value = aws_db_instance.main.address
}

// Allow us to see what the new hostname for our bastion server is.
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_lb.api.dns_name
}
